package test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import static io.restassured.RestAssured.*;
import static support.Helper.*;

import java.util.List;
import java.util.Map;

import javax.swing.text.html.ListView;

import com.sun.corba.se.spi.ior.iiop.GIOPVersion;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import support.*;

public class TestTrelloAPI {
	public static String Url = "https://api.trello.com/1/actions/592f11060f95a3d3d46a987a";

	@Test
	public void TestREST_API() {
		Response response1 = fazerGetRequest("https://api.trello.com/1/actions/592f11060f95a3d3d46a987a");
		Map<String, String> list = response1.jsonPath().getMap("data.list");

		Map<String, String> list2;
		try {

			list2 = given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).when().get(Url).then()
					.extract().response().jsonPath().get();

//			System.out.println(response.getBody().asString());
			System.out.println("Name: " + list.get("name"));
			System.out.println("List2: " + list2.get("display.entities.card.type"));

			Assert.assertTrue("StatusCode deveria ser 200", response1.statusCode() == 200);
			Assert.assertEquals(200, response1.statusCode());
			System.out.println("StatusCode: " + response1.statusCode());

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}

	}

	@Test
	public void jsonVariosNiveis() {
		try {
			Map<String, String> 
			lista = given()
					.when()
						.get(Url)
					.then()
						.statusCode(200)
						.body("display.entities.card.type", is("card"))
//						.extract().jsonPath().getMap("display.entities.card")
						.extract().jsonPath().getMap("display.entities.card")
					;
			System.out.println("Nivel json: " + lista.get("text"));
			
		}catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}
	}

}
